(function() {
	angular.module('pyoopilFrontend').controller('loginCtrl', loginCtrl);

	function loginCtrl(publicService,userService, $state, toastService) {
		var user = this;
		user.rememberMe=true;
		user.userLogin = function() {
			publicService.login(user).then(function(response) {
				userService.saveLoggedInUser(response);
				toastService.show('Logged in successfully');
				$state.go('main.dashboard');
			}, function(error) {
				toastService.showError(error.data.errors[0]);
			});
		};
	}
})();