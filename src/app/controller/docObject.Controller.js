(function() {
  angular.module('pyoopilFrontend').controller('docObjectCtrl', docObjectCtrl);

  function docObjectCtrl(appService, toastService, $scope,$sce) {
    var docObject = this;
    docObject.objectToBeEdited = {};
    if(!$scope.item.urlThumb)
    $scope.item.filePreview=$scope.app.setDoc($scope.item.mimeType);
    $scope.item.docPath=$sce.trustAsResourceUrl("http://docs.google.com/viewer?url=" + encodeURIComponent($scope.item.url))
    //set doc object cover
    docObject.setDocObjectIcon = function(files, $errors) {
      if (angular.isDefined($errors) && ($errors.filesize.length || $errors.incorrectFile.length)) {
        var docError = '';
        if ($errors.filesize.length) {
          docError += 'File size exceeds 5 mb for ' + $errors.filesize.join(', ');
        }
        if ($errors.incorrectFile.length) {
          docError += '\n Incorrect file type for ' + $errors.incorrectFile.join(', ');
        }
        toastService.showError(docError);
      } else {
        docObject.objectToBeEdited.file = files[0];
        docObject.objectToBeEdited.filePreview = $scope.app.setDoc(files[0]);
        if (!$scope.$$phase) $scope.$apply();
      }
    }
    docObject.showObjectEditing = function() {
      docObject.objectToBeEdited = angular.copy($scope.item);
      $scope.item.edit = true;
    }
    docObject.cancelObjectEditing = function() {
      $scope.item.edit = false;
    }
    docObject.editObject = function() {
      appService.editDocObject(docObject.objectToBeEdited.title, docObject.objectToBeEdited.desc, docObject.objectToBeEdited.file, $scope.item._id).then(function(response) {
        $scope.item = response;
        docObject.cancelObjectEditing();
      }, function(error) {
        if (error.data) {
          toastService.showError(error.data.errors[0]);
        }
      });
    }
  }
})();
