(function() {
    angular.module('pyoopilFrontend').controller('addImageObjectCtrl', addImageObjectCtrl);

    function addImageObjectCtrl(appService, toastService, $scope, $stateParams) {
        var addImageObject = this;
        var newImageObject = {
            title: '',
            coverPhoto: 'assets/images/upload-image.png',
            desc: '',
            file: ''
        };
        addImageObject.imageObject = angular.copy(newImageObject)
            //add new image object
        addImageObject.setImageObjectCover = function(files, $errors) {
            if (angular.isDefined($errors) && ($errors.filesize.length || $errors.incorrectFile.length)) {
                var imageError = '';
                if ($errors.filesize.length) {
                    imageError += 'File size exceeds 5 mb for ' + $errors.filesize.join(', ');
                }
                if ($errors.incorrectFile.length) {
                    imageError += '\n Incorrect file type for ' + $errors.incorrectFile.join(', ');
                }
                toastService.showError(imageError);
            } else {
                $scope.app.setImage(files[0]).then(function(result) {
                    addImageObject.imageObject.file = files[0];
                    addImageObject.imageObject.coverPhoto = result;
                    if (!$scope.$$phase) $scope.$apply();
                })
            }
        };
        addImageObject.cancelImageAdding = function() {
            addImageObject.imageObject = angular.copy(newImageObject);
            $scope.addConcepts.showAddImage = false;
        };
        addImageObject.addImageObject = function() {
          /** @namespace $scope.addConcepts */
          appService.addImageObject(addImageObject.imageObject.title, addImageObject.imageObject.desc,
              addImageObject.imageObject.file, $scope.addConcepts.selected._id).then(function(response) {
                $scope.addConcepts.selected.objects.push(response.data);
                addImageObject.cancelImageAdding();
            }, function(error) {
                if (error.data) {
                    toastService.showError(error.data.message);
                }
            });
        }
    }
})();
