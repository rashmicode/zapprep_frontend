(function() {
  angular.module('pyoopilFrontend').controller('addConceptsCtrl', addConceptsCtrl);

  function addConceptsCtrl($anchorScroll, $location, appService, $state, $stateParams, toastService, $scope, $filter, $mdBottomSheet, $mdToast, $mdDialog, $http, userService) {
    var addConcepts = this;
    var pathId = $stateParams.pathId;
    var conceptStartIndex = '';
    var conceptStartList = [];
    var objectStartIndex = '';
    var objectStartList = [];
    addConcepts.settingsToBeEdited = {};
    //get path for course settings
    appService.getPath(pathId).then(function(response) {
      addConcepts.path = response.data;
      if(userService.userId == addConcepts.path.producer._id){
        addConcepts.path['isOwner'] = true;
        addConcepts.conceptBarHeight='75vh';
      } else addConcepts.conceptBarHeight='90vh';
    }, function(error) {
      if (error.data) toastService.showError(error.data.errors[0]);
    });
    //get all concepts for path
    appService.getConceptsForPath(pathId).then(function(response) {
      // if (!response.mentorAccess) {
      //   toastService.showError('Not Authorised');
      //   $state.go('main.dashboard');
      // } else {
        addConcepts.availableConcepts = response.data;
        angular.forEach(addConcepts.availableConcepts, function(value) {
          value.showMenu = false;
          value['isOwner'] = userService.userId == value.producer;
          value.pos = parseFloat(value.pos);
        });
        addConcepts.selected = addConcepts.availableConcepts[0];
      // }
    }, function(error) {
      if (error.data) toastService.showError(error.data.errors[0]);
    });
    //get original drag start index
    addConcepts.conceptDragStart = function($index) {
        addConcepts.isConcept = true;
        conceptStartIndex = $index;
        conceptStartList = angular.copy(addConcepts.availableConcepts);
      };
      //edit concept if concept is succesffuly shuffled
    addConcepts.conceptShuffled = function(newIndex, item) {
        //to correctly index the item drop drop issue solve
        if (conceptStartIndex < newIndex) newIndex -= 1;
        if (newIndex == conceptStartIndex) {
          addConcepts.availableConcepts.splice(conceptStartIndex, 1);
          return;
        }
        if (newIndex == 0) {
          addConcepts.availableConcepts = angular.copy(conceptStartList);
          conceptStartList = [];
          toastService.showError('Not allowed');
          return;
        }
        if (conceptStartIndex < newIndex) {
          addConcepts.availableConcepts.splice(conceptStartIndex, 1);
        } else {
          addConcepts.availableConcepts.splice(conceptStartIndex + 1, 1);
        }
        // addConcepts.availableConcepts.splice(conceptStartIndex,1);
        //to correctly set new position
        var pos = '';
        if (conceptStartIndex < newIndex) {
          pos = (parseFloat(conceptStartList[newIndex].pos) + parseFloat(conceptStartList[newIndex + 1].pos)) / 2;
        } else if (newIndex == conceptStartList.length - 1) {
          pos = parseFloat(conceptStartList[conceptStartList.length - 1].pos) + 10000;
        } else {
          pos = (parseFloat(conceptStartList[newIndex].pos) + parseFloat(conceptStartList[newIndex - 1].pos)) / 2;
        }
        var data = {
          title: item.title,
          pos: pos,
          id: item._id
        };
        appService.editConcept(data).then(function(response) {
          item.pos = data.pos;
          toastService.show('Concept shuffled successfully');
        }, function(error) {
          addConcepts.availableConcepts = angular.copy(conceptStartList);
        });
      }
      //   //remove item from drag start position if item is successfully moved
      // addConcepts.objectMoved = function($index) {
      //     addConcepts.selected.objects.splice($index, 1);
      //   }
      //get original drag start index
    addConcepts.objectDragStart = function($index) {
      addConcepts.isConcept=false;
        objectStartIndex = $index;
        objectStartList = angular.copy(addConcepts.selected.objects);
      }
      //edit object if object is succesffuly shuffled
    addConcepts.objectShuffled = function(newIndex, item) {
        //to correctly index the item drop drop issue solve
        if (objectStartIndex < newIndex) newIndex -= 1;
        if (newIndex == objectStartIndex) {
          addConcepts.selected.objects.splice(objectStartIndex, 1);
          return;
        }
        if (conceptStartIndex < newIndex) {
          addConcepts.selected.objects.splice(objectStartIndex, 1);
        } else {
          addConcepts.selected.objects.splice(objectStartIndex + 1, 1);
        }
        //to correctly set new position
        var pos = '';
        if (newIndex == 0) {
          pos = (0 + parseFloat(objectStartList[0].pos)) / 2;
        } else if (objectStartIndex < newIndex) {
          pos = (parseFloat(objectStartList[newIndex].pos) + parseFloat(objectStartList[newIndex + 1].pos)) / 2;
        } else if (newIndex == objectStartList.length - 1) {
          pos = parseFloat(objectStartList[objectStartList.length - 1].pos) + 10000;
        } else {
          pos = (parseFloat(objectStartList[newIndex].pos) + parseFloat(objectStartList[newIndex - 1].pos)) / 2;
        }
        var data = {
          pos: pos,
          id: item._id
        };
        appService.editObject(data).then(function(response) {
          item.pos = data.pos;
          toastService.show('Object shuffled successfully');
        }, function(error) {
          if (error.data) toastService.showError(error.data.errors[0]);
        });
      }
      //show menu on hover
    addConcepts.showMenu = function(context) {
        if (context.title == 'Introduction') return;
        context.showMenu = true;
      }
      // hide menu on mouse over
    addConcepts.hideMenu = function(context) {
        if (context.title == 'Introduction') return;
        context.showMenu = false;
      }
      //show edit concept
    addConcepts.showEditConcept = function(concept) {
        concept.showEditConcept = true;
        addConcepts.conceptTitleToEdit = concept.title;
      }
      //edit concept
    addConcepts.editConcept = function(index, concept) {
        var data = {
          title: addConcepts.conceptTitleToEdit,
          pos: concept.pos,
          id: concept._id
        };
        appService.editConcept(data).then(function(response) {
          concept.title = response.data.title;
          addConcepts.cancelEditConcept(concept);
          toastService.show('Concept Title edited Successfully');
        }, function(error) {});
      }
      //show delete concept dialog
    addConcepts.deleteConcept = function(ev, index) {
      var confirm = $mdDialog.confirm().title('Are you sure you want to delete this concept?').ariaLabel('Delete concept').targetEvent(ev).ok('Delete').cancel('Cancel');
      $mdDialog.show(confirm).then(function() {
        appService.deleteConcept(addConcepts.availableConcepts[index]._id).then(function() {
          addConcepts.availableConcepts.splice(index, 1);
          toastService.show('Concept deleted successfully.');
        })
      });
    }
    addConcepts.deleteObject = function(ev, index) {
        var confirm = $mdDialog.confirm().title('Are you sure you want to delete this object?').ariaLabel('Delete object').targetEvent(ev).ok('Delete').cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
          appService.deleteObject(addConcepts.selected.objects[index]._id).then(function() {
            addConcepts.selected.objects.splice(index, 1);
            toastService.show('Object deleted successfully.');
          })
        });
      }
      //cancel concept editing
    addConcepts.cancelEditConcept = function(concept) {
        concept.showEditConcept = false;
        addConcepts.conceptTitleToEdit = '';
      }
      //add new concept
    addConcepts.addConcept = function() {
        var title = addConcepts.newConceptTitle;
        appService.addConcept(title, pathId).then(function(response) {
          toastService.show('Concept added successfully');
          addConcepts.availableConcepts.push(response.data);
          addConcepts.newConceptTitle = '';
          addConcepts.addConceptForm.$setPristine();
          addConcepts.addConceptForm.$setUntouched();
          addConcepts.addConceptForm.$setValidity();
          $location.hash('lastConcept');
          $anchorScroll();
        });
      }

      //show edit course settings
    addConcepts.showCourseSettingsEditing = function() {
        addConcepts.settingsToBeEdited = angular.copy(addConcepts.path);
        if (addConcepts.settingsToBeEdited.coursePaid) {
          addConcepts.settingsToBeEdited.coursePriceAmount = addConcepts.settingsToBeEdited['course' + addConcepts.settingsToBeEdited.selectedCurrency];
        }
        if (addConcepts.settingsToBeEdited.mentorPaid) {
          addConcepts.settingsToBeEdited.mentorPriceAmount = addConcepts.settingsToBeEdited['mentor' + addConcepts.settingsToBeEdited.selectedCurrency];
        }
        addConcepts.showEditCourseSettings = true;
      };

      addConcepts.showCourseSettingsDelete = function(ev) {
          var confirm = $mdDialog.confirm().title('Are you sure you want to delete this course?').ariaLabel('Delete course').targetEvent(ev).ok('Delete').cancel('Cancel');
          $mdDialog.show(confirm).then(function() {
              appService.deletePath(addConcepts.path._id).then(function() {
                  toastService.show('Course deleted successfully.');
                  $state.go('main.dashboard');
              })
          });
      };
      //cancel course settings editing
    addConcepts.cancelCourseSettingsEditing = function() {
        addConcepts.settingsToBeEdited = {};
        addConcepts.showEditCourseSettings = false;
        addConcepts.editCourseSettingsForm.$setPristine();
      };
      //settings image cover
    addConcepts.setCourseSettingsCover = function(files, $errors) {
      if (angular.isDefined($errors) && ($errors.filesize.length || $errors.incorrectFile.length)) {
        var imageError = '';
        if ($errors.filesize.length) {
          imageError += 'File size exceeds 5 mb for ' + $errors.filesize.join(', ');
        }
        if ($errors.incorrectFile.length) {
          imageError += '\n Incorrect file type for ' + $errors.incorrectFile.join(', ');
        }
        toastService.showError(imageError);
      } else {
        $scope.app.setImage(files[0]).then(function(result) {
          addConcepts.settingsToBeEdited.uploadedPhoto = files[0];
          addConcepts.settingsToBeEdited.coverPhoto = result;
          if (!$scope.$$phase) $scope.$apply();
        })
      }
    };
      addConcepts.categories = {
            'CAT': ['Quants', 'Verbal', 'LR/DI', 'Admission Interview', 'Notice Board','Video Lessons','Test Series'],
            'UPSC': ['Current Affairs', 'History', 'Geography', 'Political Science', 'Economics', 'Reasoning',
                'Environment and Ecology', 'Science', 'Art and Culture', 'Interview', 'Notice Board','Video Lessons','Test Series'],
            'SSC': ['General Intelligence and Reasoning', 'General Awareness', 'Quantitative Aptitude', 'English Language',
                'Interview', 'Notice Board','Video Lessons','Test Series'],
            'IBPS': ['Reasoning', 'Quants', 'English', 'General Awareness', 'Computer', 'Interview','Video Lessons','Test Series'],
            'Placements': ['Quants', 'Verbal', 'LR/DI', 'HR Interview', 'Other Courses', 'Notice Board', 'Company Wise Preparation','Video Lessons','Test Series'],
            'GATE': ['Quants', 'ME', 'EEE', 'ECE', 'CE', 'CS', 'Maths', 'Notice Board','Video Lessons','Test Series'],
            'NDA': ['Mathematics', 'English', 'General Science', 'Social Studies', 'Current Affairs', 'SSB', 'Notice Board','Video Lessons','Test Series'],
            '12th Board': ['Maths', 'Physics', 'Biology', 'Chemistry', 'English', 'Social Science','Video Lessons','Test Series'],
            'NEET': ['Physics', 'Chemistry', 'Biology', 'Admission Counselling', 'Notice Board','Video Lessons','Test Series'],
            'JEE': ['Physics', 'Chemistry', 'Maths', 'Admission Counselling', 'Notice Board','Video Lessons','Test Series'],
            'SAT': ['Critical Reading / Writing', 'Maths', 'Science', 'Interview','Video Lessons','Test Series'],
            'GRE': ['Verbal', 'Quants', 'Interview','Video Lessons','Test Series'],
            'GMAT': ['Integrated Reasoning', 'Quantitative', 'Verbal', 'Interview','Video Lessons','Test Series'],
            '10th Board': ['Maths', 'Physics', 'Chemistry', 'Biology', 'English', 'Social Science','Video Lessons','Test Series']
        };
    addConcepts.editCourseSettings = function() {
        appService.editPath(addConcepts.settingsToBeEdited).then(function(response) {
          addConcepts.path = response.data;
          addConcepts.showEditCourseSettings = false;
          toastService.show('Course Edited Successfully');
        }, function(error) {});
      };
      //add object
    addConcepts.showGridBottomSheet = function($event) {
      // $scope.alert = '';
      addConcepts.showAddImage = false;
      addConcepts.showAddVideo = false;
      addConcepts.showAddText = false;
      addConcepts.showAddLink = false;
      addConcepts.showAddDoc = false;
      $mdBottomSheet.show({
        templateUrl: 'app/views/conceptAddTool.html',
        controller: function($mdBottomSheet) {
          var bottomSheet = this;
          bottomSheet.items = [{
            name: 'Image',
            icon: 'zmdi-image'
          }, {
            name: 'Text',
            icon: 'zmdi-format-size'
          }, {
            name: 'Video',
            icon: 'zmdi-videocam'
          }, {
            name: 'Document',
            icon: 'zmdi-assignment-o'
          }, {
            name: 'Link',
            icon: 'zmdi-link'
          }, {
            name: 'Quiz',
            icon: 'zmdi-format-list-numbered'
          }, {
            name: 'Preview',
            icon: 'zmdi-eye'
          }];
          bottomSheet.listItemClick = function($index) {
            var clickedItem = bottomSheet.items[$index];
            $mdBottomSheet.hide(clickedItem);
          };
        },
        controllerAs: 'bottomSheet',
        targetEvent: $event
      }).then(function(clickedItem) {
        if (clickedItem.name != 'Quiz' && clickedItem.name != 'Preview') {
          $location.hash('lastObject');
          $anchorScroll();
          $anchorScroll.yOffset = 500;
        }
        switch (clickedItem.name) {
          case 'Image':
            addConcepts.showAddImage = true;
            break;
          case 'Text':
            addConcepts.showAddText = true;
            break;
          case 'Video':
            addConcepts.showAddVideo = true;
            break;
          case 'Link':
            addConcepts.showAddLink = true;
            break;
          case 'Document':
            addConcepts.showAddDoc = true;
            break;
          case 'Preview':
            $mdDialog.show({
              controller: 'conceptPreviewCtrl',
              controllerAs: 'conceptPreview',
              clickOutsideToClose: true,
              templateUrl: 'app/views/conceptPreview.html',
              parent: angular.element(document.body)
            }).then(function(answer) {
              $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.status = 'You cancelled the dialog.';
            });
            break;
        }
      });
    };
  }
})();
