(function() {
    angular.module('pyoopilFrontend').controller('addDocObjectCtrl', addDocObjectCtrl);

    function addDocObjectCtrl(appService, toastService, $scope, $stateParams) {
        var addDocObject = this;
        var newDocObject = {
            title: '',
            coverPhoto: 'assets/images/upload-image.png',
            desc: '',
            file: ''
        };
        addDocObject.docObject = angular.copy(newDocObject)
            //add new doc object
        addDocObject.setDocObjectIcon = function(files, $errors) {
            if (angular.isDefined($errors) && ($errors.filesize.length || $errors.incorrectFile.length)) {
                var docError = '';
                if ($errors.filesize.length) {
                    docError += 'File size exceeds 5 mb for ' + $errors.filesize.join(', ');
                }
                if ($errors.incorrectFile.length) {
                    docError += '\n Incorrect file type for ' + $errors.incorrectFile.join(', ');
                }
                toastService.showError(docError);
            } else {
                addDocObject.docObject.file = files[0];
                addDocObject.docObject.coverPhoto = $scope.app.setDoc(files[0]);
                if (!$scope.$$phase) $scope.$apply();
            }
        };
        addDocObject.cancelDocAdding = function() {
            addDocObject.docObject = angular.copy(newDocObject);
            $scope.addConcepts.showAddDoc = false;
        };
        addDocObject.addDocObject = function() {
            appService.addDocObject(addDocObject.docObject.title, addDocObject.docObject.desc,
              addDocObject.docObject.file, $scope.addConcepts.selected._id).then(function(response) {
                $scope.addConcepts.selected.objects.push(response.data);
                addDocObject.cancelDocAdding();
            }, function(error) {
                if (error.data) {
                    toastService.showError(error.data.message);
                }
            });
        }
    }
})();
