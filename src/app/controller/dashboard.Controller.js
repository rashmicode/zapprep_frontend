(function() {
    angular.module('pyoopilFrontend').controller('dashboardCtrl', dashboardCtrl);

    function dashboardCtrl(appService, $state, userService, $mdDialog) {
        var dashboard = this;
        appService.getPathsForUser().then(function(response) {
            dashboard.dashData = response.data;
        }, function(error) {});
        dashboard.goToCreateCourse = function() {
            $state.go('main.createCourse.step1');
        };
        dashboard.goToConcept = function(dash) {
            // if(userService._id == dash.producer._id) {
                $state.go('main.addConcepts', {
                    'pathId': dash._id
                });
            // } else {
            //     $mdDialog.show({
            //         controller: function ($scope, $mdDialog) {
            //             $scope.cancel = function() {
            //                 $mdDialog.cancel();
            //             };
            //         },
            //         templateUrl: 'app/views/restrictDash.html',
            //         parent: angular.element(document.body),
            //         clickOutsideToClose: true
            //     });
            // }
        }
    }
})();
