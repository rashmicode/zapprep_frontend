(function() {
  angular.module('pyoopilFrontend').controller('linkObjectCtrl', linkObjectCtrl);

  function linkObjectCtrl(appService, toastService, $scope) {
    var linkObject = this;
    appService.getPreview($scope.item.url).then(function(response) {
      if (response.image) $scope.item.previewImage = response.image;
    }, function(error) {
      if (error.data) toastService.showError(error.data.errors[0]);
    })
    linkObject.objectToBeEdited = {};
    linkObject.showObjectEditing = function() {
      linkObject.objectToBeEdited = angular.copy($scope.item);
      $scope.item.edit = true;
      linkObject.show = true;
    }
    linkObject.cancelObjectEditing = function() {
      $scope.item.edit = false;
    }
    linkObject.editObject = function() {
      appService.editLinkObject(linkObject.objectToBeEdited.title, linkObject.objectToBeEdited.desc, linkObject.objectToBeEdited.url, $scope.item._id).then(function(response) {
        $scope.item = response;
        linkObject.cancelObjectEditing();
      }, function(error) {
        if (error.data) {
          toastService.showError(error.data.errors[0]);
        }
      });
    }
    linkObject.getPreview = function() {
      if (linkObject.objectToBeEdited.url) {
        linkObject.show = true;
        linkObject.previewImage = '';
        appService.getPreview(linkObject.objectToBeEdited.url).then(function(response) {
          if (response.image) linkObject.previewImage = response.image;
          linkObject.objectToBeEdited.title = response.title;
          linkObject.objectToBeEdited.desc = response.description;
        }, function(error) {
          if (error.data) toastService.showError(error.data.errors[0]);
        })
      } else {
        linkObject.show = false;
        linkObject.previewImage = '';
        linkObject.objectToBeEdited.title = '';
        linkObject.objectToBeEdited.desc = '';
      }
    }
  }
})();
