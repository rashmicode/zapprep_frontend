(function() {
	angular.module('pyoopilFrontend').controller('createCourseCtrl', createCourseCtrl);

	function createCourseCtrl(toastService, $scope, $state, appService) {
		var createCourse = this;
		createCourse.data = {};
		createCourse.categories = {
            'CAT': ['Quants', 'Verbal', 'LR/DI', 'Admission Interview', 'Notice Board','Video Lessons','Test Series'],
            'UPSC': ['Current Affairs', 'History', 'Geography', 'Political Science', 'Economics', 'Reasoning',
                'Environment and Ecology', 'Science', 'Art and Culture', 'Interview', 'Notice Board','Video Lessons','Test Series'],
            'SSC': ['General Intelligence and Reasoning', 'General Awareness', 'Quantitative Aptitude', 'English Language',
                'Interview', 'Notice Board','Video Lessons','Test Series'],
            'IBPS': ['Reasoning', 'Quants', 'English', 'General Awareness', 'Computer', 'Interview','Video Lessons','Test Series'],
            'Placements': ['Quants', 'Verbal', 'LR/DI', 'HR Interview', 'Other Courses', 'Notice Board', 'Company Wise Preparation','Video Lessons','Test Series'],
            'GATE': ['Quants', 'ME', 'EEE', 'ECE', 'CE', 'CS', 'Maths', 'Notice Board','Video Lessons','Test Series'],
            'NDA': ['Mathematics', 'English', 'General Science', 'Social Studies', 'Current Affairs', 'SSB', 'Notice Board','Video Lessons','Test Series'],
            '12th Board': ['Maths', 'Physics', 'Biology', 'Chemistry', 'English', 'Social Science','Video Lessons','Test Series'],
            'NEET': ['Physics', 'Chemistry', 'Biology', 'Admission Counselling', 'Notice Board','Video Lessons','Test Series'],
            'JEE': ['Physics', 'Chemistry', 'Maths', 'Admission Counselling', 'Notice Board','Video Lessons','Test Series'],
            'SAT': ['Critical Reading / Writing', 'Maths', 'Science', 'Interview','Video Lessons','Test Series'],
            'GRE': ['Verbal', 'Quants', 'Interview','Video Lessons','Test Series'],
            'GMAT': ['Integrated Reasoning', 'Quantitative', 'Verbal', 'Interview','Video Lessons','Test Series'],
            '10th Board': ['Maths', 'Physics', 'Chemistry', 'Biology', 'English', 'Social Science','Video Lessons','Test Series']
        };
		createCourse.uploadedPhoto = '';
		createCourse.step1FormCopy = '';
		createCourse.checkIsStep1 = function() {
			createCourse.isStep1 = $state.current.name == 'main.createCourse.step1';
		};
		createCourse.coverPhoto = 'assets/images/upload-image.png';
		createCourse.setCover = function(files, $errors) {
			if (angular.isDefined($errors) && ($errors.filesize.length || $errors.incorrectFile.length)) {
				var imageError = '';
				if ($errors.filesize.length) {
					imageError += 'File size exceeds 5 mb for ' + $errors.filesize.join(', ');
				}
				if ($errors.incorrectFile.length) {
					imageError += '\n Incorrect file type for ' + $errors.incorrectFile.join(', ');
				}
				toastService.showError(imageError);
			} else {
				$scope.app.setImage(files[0]).then(function(result) {
					createCourse.data.uploadedPhoto = files[0];
					createCourse.coverPhoto = result;
					if (!$scope.$$phase) $scope.$apply();
				})
			}
		};
		createCourse.goToStep2 = function() {
			createCourse.step1FormCopy = angular.copy(createCourse.step1Form);
			$state.go('main.createCourse.step2');
		};
		createCourse.goToStep1 = function() {
			$state.go('main.createCourse.step1');
		};
		createCourse.createCourse = function() {
			appService.createPath(createCourse.data).then(function(response) {
				$state.go('main.addConcepts', {
					'pathId': response.data._id
				});
				toastService.show('Course created successfully');
			}, function(error) {
				toastService.showError(error.data.error[0]);
			});
		}
	}
})();
