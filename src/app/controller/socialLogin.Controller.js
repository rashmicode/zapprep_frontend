(function() {
	angular.module('pyoopilFrontend').controller('socialLoginCtrl', socialLoginCtrl);

	function socialLoginCtrl($auth, $state, userService, toastService) {
		var socialLogin = this;
		socialLogin.authenticate = function(provider) {
			$auth.authenticate(provider).then(function(response) {
				userService.saveLoggedInUser(response.data);
				toastService.show('You have successfully signed in with ' + provider + '!');
				$state.go('main.dashboard');
			}).catch(function errorCallback(error) {
                setTimeout(function() {
                    if (error.error) {
                        // Popup error - invalid redirect_uri, pressed cancel button, etc.
                        toastService.showError(error.error);
                    } else if (error.data) {
                        // HTTP response error from server
                        toastService.showError(error.data.message, error.status);
                    } else {
                        toastService.showError(error);
                    }
                    throw error;
                }, 10);
                //using setTimeout moves the actual throwingout of the
                // promise chain. It's kind of a hack!
                return $q.reject(error); //reReject the error, so the rest
                // of the chain knows there is something wrong
            });
		};
	}
})();