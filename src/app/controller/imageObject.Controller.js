(function() {
  angular.module('pyoopilFrontend').controller('imageObjectCtrl', imageObjectCtrl);

  function imageObjectCtrl(appService, toastService, $scope) {
    var imageObject = this;
    imageObject.objectToBeEdited = {};
    //set image object cover
    imageObject.setObjectCover = function(files, $errors) {
      if (angular.isDefined($errors) && ($errors.filesize.length || $errors.incorrectFile.length)) {
        var imageError = '';
        if ($errors.filesize.length) {
          imageError += 'File size exceeds 5 mb for ' + $errors.filesize.join(', ');
        }
        if ($errors.incorrectFile.length) {
          imageError += '\n Incorrect file type for ' + $errors.incorrectFile.join(', ');
        }
        toastService.showError(imageError);
      } else {
        $scope.app.setImage(files[0]).then(function(result) {
          imageObject.objectToBeEdited.file = files[0];
          imageObject.objectToBeEdited.url = result;
          if (!$scope.$$phase) $scope.$apply();
        })
      }
    }
    imageObject.showObjectEditing = function() {
      imageObject.objectToBeEdited = angular.copy($scope.item);
      $scope.item.edit = true;
    }
    imageObject.cancelObjectEditing = function() {
      $scope.item.edit = false;
    }
    imageObject.editObject = function() {
      appService.editImageObject(imageObject.objectToBeEdited.title, imageObject.objectToBeEdited.desc, imageObject.objectToBeEdited.file,$scope.item._id).then(function(response) {
        $scope.item=response.data;
        imageObject.cancelObjectEditing();
      }, function(error) {
        if (error.data) {
          toastService.showError(error.data.errors[0]);
        }
      });
    }
  }
})();
