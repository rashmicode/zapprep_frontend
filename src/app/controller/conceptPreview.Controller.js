(function() {
	angular.module('pyoopilFrontend').controller('conceptPreviewCtrl', conceptPreviewCtrl);

	function conceptPreviewCtrl(appService, toastService, $stateParams) {
		var conceptPreview = this;
		var pathId = $stateParams.pathId;
		appService.getConceptsForPath(pathId).then(function(response) {
			if (!response.mentorAccess) {
				toastService.showError('Not Authorised');
				$state.go('main.dashboard');
			} else {
				conceptPreview.concepts = response.data;
			}
		}, function(error) {
			if (error.data) toastService.showError(error.data.errors[0]);
		});
	}
})();