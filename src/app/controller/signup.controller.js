(function() {
	angular.module('pyoopilFrontend').controller('signupCtrl', signupCtrl);

	function signupCtrl(userService,publicService, toastService, $state) {
		var sign = this;
		sign.signup = function() {
			publicService.signup(sign.email, sign.username, sign.password, sign.name, sign.phone).then(function(response) {
				userService.saveLoggedInUser(response);
				toastService.show('Signed up successfully');
				$state.go('main.dashboard');
			}, function(error) {
				toastService.showError(error.data.errors[0]);
			})
		}
	}
})();