(function() {
  angular.module('pyoopilFrontend').controller('textObjectCtrl', textObjectCtrl);

  function textObjectCtrl(appService, toastService, $scope) {
    var textObject = this;
    textObject.objectToBeEdited = {};

    textObject.showObjectEditing = function() {
      textObject.objectToBeEdited = angular.copy($scope.item);
      $scope.item.edit = true;
    };
    textObject.cancelObjectEditing = function() {
      $scope.item.edit = false;
    };
    textObject.editObject = function() {
      appService.editTextObject(textObject.objectToBeEdited.title, textObject.objectToBeEdited.desc, $scope.item._id).then(function(response) {
        $scope.item=response.data;
        textObject.cancelObjectEditing();
      }, function(error) {
        if (error.data) {
          toastService.showError(error.data.errors[0]);
        }
      });
    }
  }
})();
