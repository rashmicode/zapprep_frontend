(function() {
	angular.module('pyoopilFrontend').controller('mainAppCtrl', mainAppCtrlFunc);

	function mainAppCtrlFunc($rootScope, $state, $q, youtubeRegex, emailRegex, userService) {
		var app = this;
		$rootScope.$on('userAuthenticated', function() {
			$state.transitionTo('main.dashboard');
		});
		$rootScope.$on('userUnauthenticated', function() {
      // userService.removeParameters();
			$state.transitionTo('login');
		});
		app.youtubeRegex = youtubeRegex;
		app.emailRegex = emailRegex;
		app.setImage = function(file) {
			var defer = $q.defer();
			var reader = new FileReader();
			reader.onload = function(event) {
					defer.resolve(event.target.result);
				}
				// when the file is read it triggers the onload event above.
			reader.readAsDataURL(file);
			return defer.promise;
		}
		app.isEmpty = function(obj) {
			for (var prop in obj) {
				if (obj.hasOwnProperty(prop)) return false;
			}
			return true;
		}
		app.setDoc = function(file) {
			var fileType = ''
			if (angular.isString(file)) {
				fileType = file;
			} else {
				fileType = file.type;
			}
			var doc = '/assets/images/fileIcons/doc_file_type.png';
			var excel = '/assets/images/fileIcons/excel_file_type.png';
			var file = '/assets/images/fileIcons/file_type.png';
			var pdf = '/assets/images/fileIcons/pdf_file_type.png';
			var ppt = '/assets/images/fileIcons/ppt_file_type.png';
			switch (fileType) {
				case "application/msword":
				case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
				case "application/vnd.openxmlformats-officedocument.wordprocessingml.template":
				case "application/vnd.oasis.opendocument.text":
					return doc;
				case "application/vnd.ms-excel":
				case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
				case "application/vnd.openxmlformats-officedocument.spreadsheetml.template":
					return excel;
				case "application/vnd.openxmlformats-officedocument.presentationml.template":
				case "application/vnd.openxmlformats-officedocument.presentationml.slideshow":
				case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
				case "application/vnd.openxmlformats-officedocument.presentationml.slide":
				case "application/mspowerpoint":
				case "application/powerpoint":
				case "application/vnd.ms-powerpoint":
				case "application/x-mspowerpoint":
					return ppt;
				case "application/pdf":
					return pdf;
				default:
					return file;
			}
		}
	}
})();
