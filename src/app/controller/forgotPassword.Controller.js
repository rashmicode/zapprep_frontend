(function() {
  angular.module('pyoopilFrontend').controller('forgotPasswordCtrl', forgotPasswordCtrl);

  function forgotPasswordCtrl(publicService, $mdDialog, $state) {
    var forgotPassword = this;
    forgotPassword.sendEmail = function() {
      publicService.forgotPassword(forgotPassword.email).then(function(response) {
        var alert = $mdDialog.alert({
          title: 'An email has been sent to your registered email address.',
          textContent: '',
          ok: 'Login'
        });
        $mdDialog.show(alert).finally(function() {
          $state.go('login');
        });
      });
    }
  }
})();