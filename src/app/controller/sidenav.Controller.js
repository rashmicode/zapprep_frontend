(function() {
	angular.module('pyoopilFrontend').controller('sidenavCtrl', sidenavCtrl);

	function sidenavCtrl(localStorageService, $state, $rootScope, userService, $mdSidenav) {
		var sidenav = this;
		sidenav.sideAvatar = userService.avatar;
		sidenav.sideName = userService.name;
		sidenav.sideUsername = userService.username;
		sidenav.userId = userService.userId;
		$rootScope.$on('userAuthenticated', function() {
			sidenav.sideAvatar = userService.avatar;
			sidenav.sideName = userService.name;
			sidenav.sideUsername = userService.username;
			sidenav.userId = userService.userId;
		})
		sidenav.goToDashboard=function(){
			$mdSidenav('menu').close();
			$state.go('main.dashboard');
		}
		sidenav.logout = function() {
			$mdSidenav('menu').close();
			userService.removeParameters();
		}
	}
})();