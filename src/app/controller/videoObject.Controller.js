(function() {
  angular.module('pyoopilFrontend').controller('videoObjectCtrl', videoObjectCtrl);

  function videoObjectCtrl(appService, toastService, $scope) {
    var videoObject = this;
    videoObject.objectToBeEdited = {};
    $scope.$watch('videoObject.objectToBeEdited.url', function(url) {
      if (videoObject.editObjectForm && videoObject.editObjectForm.videoUrl.$dirty) {
        if (url) {
          videoObject.getPreview();
        } else {
          videoObject.objectToBeEdited.title = '';
          videoObject.objectToBeEdited.desc = '';
          videoObject.show = false;
        }
      }
    });
    videoObject.showObjectEditing = function() {
      videoObject.objectToBeEdited = angular.copy($scope.item);
      $scope.item.edit = true;
      videoObject.show = true;
    };
    videoObject.cancelObjectEditing = function() {
      $scope.item.edit = false;
    };
    videoObject.editObject = function() {
      appService.editLinkObject(videoObject.objectToBeEdited.title, videoObject.objectToBeEdited.desc, videoObject.objectToBeEdited.url, $scope.item._id).then(function(response) {
        $scope.item = response.data;
        videoObject.cancelObjectEditing();
      }, function(error) {
        if (error.data) {
          toastService.showError(error.data.errors[0]);
        }
      });
    };
    videoObject.getPreview = function() {
      appService.getPreview(videoObject.objectToBeEdited.url).then(function(response) {
        videoObject.show = true;
        videoObject.objectToBeEdited.title = response.title;
        videoObject.objectToBeEdited.desc = response.description;
      }, function(error) {
        if (error.data) toastService.showError(error.data.errors[0]);
      })
    }
  }
})();
