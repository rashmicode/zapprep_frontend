(function() {
    angular.module('pyoopilFrontend').controller('addLinkObjectCtrl', addLinkObjectCtrl);

    function addLinkObjectCtrl(appService, toastService, $scope, $stateParams) {
        var addLinkObject = this;
        var newLinkObject = {
            title: '',
            desc: '',
            url: ''
        };
        addLinkObject.linkObject = angular.copy(newLinkObject)
        addLinkObject.cancelObjectAdding = function() {
            addLinkObject.linkObject = angular.copy(newLinkObject);
            $scope.addConcepts.showAddLink = false;
        }
        addLinkObject.addObject = function() {
            appService.addLinkObject(addLinkObject.linkObject.title, addLinkObject.linkObject.desc, addLinkObject.linkObject.url, $scope.addConcepts.selected._id).then(function(response) {
                $scope.addConcepts.selected.objects.push(response.data);
                addLinkObject.cancelObjectAdding();
                toastService.show('Object Added Successfully');
            }, function(error) {
                if (error.data) {
                    toastService.showError(error.data.errors[0]);
                }
            });
        }
        addLinkObject.getPreview = function() {
            if (addLinkObject.linkObject.url) {
                addLinkObject.show = true;
                addLinkObject.previewImage = '';
                appService.getPreview(addLinkObject.linkObject.url).then(function(response) {
                    if (response.image) addLinkObject.previewImage = response.image;
                    addLinkObject.linkObject.title = response.title;
                    addLinkObject.linkObject.desc = response.description;
                }, function(error) {
                    if (error.data) toastService.showError(error.data.errors[0]);
                })
            } else {
                addLinkObject.previewImage = '';
                addLinkObject.linkObject.title = '';
                addLinkObject.linkObject.desc = '';
                addLinkObject.show = false;
            }
        }
    }
})();
