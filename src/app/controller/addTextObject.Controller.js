(function() {
    angular.module('pyoopilFrontend').controller('addTextObjectCtrl', addTextObjectCtrl);

    function addTextObjectCtrl(appService, toastService, $scope) {
        var addTextObject = this;
        var newTextObject = {
            title: '',
            desc: ''
        };
        addTextObject.textObject = angular.copy(newTextObject);

        addTextObject.cancelTextAdding = function() {
            addTextObject.textObject = angular.copy(newTextObject);
            $scope.addConcepts.showAddText = false;
        };
        addTextObject.addTextObject = function() {
            appService.addTextObject(addTextObject.textObject.title, addTextObject.textObject.desc, $scope.addConcepts.selected._id).then(function(response) {
                $scope.addConcepts.selected.objects.push(response.data);
                addTextObject.cancelTextAdding();
                toastService.show('Object added successfully')
            }, function(error) {
                if (error.data) {
                    toastService.showError(error.data.errors[0]);
                }
            });
        }
    }
})();
