(function() {
  angular.module('pyoopilFrontend').controller('addVideoObjectCtrl', addVideoObjectCtrl);

  function addVideoObjectCtrl(appService, toastService, $scope, $stateParams) {
    var addVideoObject = this;
    var newVideoObject = {
      title: '',
      desc: '',
      url: ''
    };
    addVideoObject.videoObject = angular.copy(newVideoObject)
    addVideoObject.cancelObjectAdding = function() {
      addVideoObject.videoObject = angular.copy(newVideoObject);
      $scope.addConcepts.showAddVideo = false;
    }
    addVideoObject.addObject = function() {
      appService.addLinkObject(addVideoObject.videoObject.title, addVideoObject.videoObject.desc, addVideoObject.videoObject.url, $scope.addConcepts.selected._id).then(function(response) {
        $scope.addConcepts.selected.objects.push(response.data);
        addVideoObject.cancelObjectAdding();
        toastService.show('Object Added Successfully');
      }, function(error) {
        if (error.data) {
          toastService.showError(error.data.errors[0]);
        }
      });
    }
    $scope.$watch('addVideoObject.videoObject.url', function(url) {
      if (url) {
        addVideoObject.getPreview();
      } else {
        addVideoObject.videoObject.title = '';
        addVideoObject.videoObject.desc = '';
        addVideoObject.show = false;
      }
    });
    addVideoObject.getPreview = function() {
      addVideoObject.show = true;
      appService.getPreview(addVideoObject.videoObject.url).then(function(response) {
        addVideoObject.videoObject.title = response.title;
        addVideoObject.videoObject.desc = response.description;
      }, function(error) {
        if (error.data) toastService.showError(error.data.errors[0]);
      });
    }
  }
})();
