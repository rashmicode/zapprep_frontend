(function() {
    angular.module('pyoopilFrontend').factory('publicService', publicService);

    function publicService(Restangular) {
        var self = this;
        self.login = function(user) {
            var data = {
                email: user.email,
                password: user.password
            };
            return Restangular.all('user').all('login').post(data);
        };
        self.signup = function(email, username, password, name, phone) {
            var data = {
                email: email,
                password: password,
                username: username,
                name: name,
                phone: phone
            };
            return Restangular.all('user').all('signup').post(data);
        };
        self.forgotPassword=function (email) {
            var data={
                email:email
            }
            return Restangular.all('user').all('forgotPassword').post(data);
        }
        return self;
    }
})();
