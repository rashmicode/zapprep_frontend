(function() {
	angular.module('pyoopilFrontend').factory('appService', appService);

	function appService(Restangular, localStorageService, userService, $rootScope, globalService) {
		var self = this;
		var token = '';
		if (globalService.getIsAuthorised()) {
			token = localStorageService.get('token');
		}
		$rootScope.$on('userAuthenticated', function() {
			token = localStorageService.get('token');
		});
		// var token = userService.token;
		self.getPathsForUser = function() {
			return Restangular.all('user').customGET('paths', {
				access_token: token
			});
		}
		self.getConceptsForPath = function(pathId) {
			return Restangular.all('concepts').customGET('', {
				access_token: userService.token,
				pathId: pathId
			});
		}
		self.getPath = function(pathId) {
			return Restangular.all('paths').customGET('', {
				access_token: token,
				pathId: pathId
			});
		}
		self.createPath = function(createCourse) {
			var data = new FormData();
			data.append('title', createCourse.title);
			data.append('desc', createCourse.desc);
			data.append('coverPhoto', createCourse.uploadedPhoto);
			data.append('mentorPaid', createCourse.mentorPaid);
			data.append('coursePaid', createCourse.coursePaid);
			// to remove
			data.append('category', createCourse.category);
			data.append('subCat', createCourse.subCat);
			if (createCourse.coursePaid) {
				data.append('course' + createCourse.selectedCurrency, createCourse.coursePriceAmount)
			}
			if (createCourse.mentorPaid) {
				data.append('mentor' + createCourse.selectedCurrency, createCourse.mentorPriceAmount)
			}
			data.append('introVid', createCourse.introVid);
			return Restangular.all("paths").withHttpConfig({
				transformRequest: angular.identity
			}).customPOST(data, undefined, {
				access_token: token
			}, {
				'Content-Type': undefined
			});
		};
		self.editPath = function(editCourse) {
				var data = new FormData();
				data.append('pathId', editCourse.id);
				data.append('title', editCourse.title);
				data.append('desc', editCourse.desc);
				data.append('coverPhoto', editCourse.uploadedPhoto);
				data.append('mentorPaid', editCourse.mentorPaid);
				data.append('coursePaid', editCourse.coursePaid);
                data.append('isPrivate', editCourse.isPrivate);
                data.append('category', editCourse.category);
                data.append('subCat', editCourse.subCat);

            if (editCourse.coursePaid) {
					data.append('course' + editCourse.selectedCurrency, editCourse.coursePriceAmount);
				}
				if (editCourse.mentorPaid) {
					data.append('mentor' + editCourse.selectedCurrency, editCourse.mentorPriceAmount);
				}
				return Restangular.all("paths").withHttpConfig({
					transformRequest: angular.identity
				}).patch(data, {
					access_token: token
				}, {
					'Content-Type': undefined
				});
			};
			//edit position or title of concept
		self.editConcept = function(concept) {
				var data = {
					title: concept.title,
					pos: concept.pos,
					id: concept.id
				};
				return Restangular.all('concepts').patch(data, {
					access_token: token
				});
			};
			// add new concept
		self.addConcept = function(title, pathId) {
				var data = {
					title: title,
					pathId: pathId
				}
				return Restangular.all('concepts').customPOST(data, undefined, {
					access_token: token
				})
			};
			//delete concept
		self.deleteConcept = function(id) {
				var data = {
						id: id
					};
					//to send data in body for delete request
				return Restangular.all('').customOperation('remove', 'concepts', {
					access_token: token
				}, {
					"Content-Type": "application/json;charset=utf-8"
				}, data);
			};

		self.deletePath = function(id) {
				var data = {
						id: id
					};
					//to send data in body for delete request
				return Restangular.all('').customOperation('remove', 'paths', {
					access_token: token
				}, {
					"Content-Type": "application/json;charset=utf-8"
				}, data);
			};
			//delete object
		self.deleteObject = function(id) {
				var data = {
						id: id
					};
					//to send data in body for delete request
				return Restangular.all('').customOperation('remove', 'objects', {
					access_token: token
				}, {
					"Content-Type": "application/json;charset=utf-8"
				}, data);
			};
			//add image object
		self.addImageObject = function(title, desc, file, conceptId) {
				var data = new FormData();
				data.append('title', title);
				data.append('desc', desc);
				data.append('file', file);
				data.append('conceptId', conceptId);
				data.append('type', 'image');
				return Restangular.all("objects").withHttpConfig({
					transformRequest: angular.identity
				}).customPOST(data, undefined, {
					access_token: token
				}, {
					'Content-Type': undefined
				});
			};
			//edit image object
		self.editImageObject = function(title, desc, file, objectId) {
			var data = new FormData();
			data.append('title', title);
			data.append('desc', desc);
			data.append('file', file);
			data.append('id', objectId);
			data.append('type', 'image');
			return Restangular.all("objects").withHttpConfig({
				transformRequest: angular.identity
			}).patch(data, {
				access_token: token
			}, {
				'Content-Type': undefined
			});
		};
		self.addTextObject = function(title, desc, conceptId) {
				var data = {
					title: title,
					desc: desc,
					type: 'text',
					conceptId: conceptId
				};
				return Restangular.all('objects').customPOST(data, undefined, {
					access_token: token
				})
			};
			//edit text object
		self.editTextObject = function(title, desc, objectId) {
				var data = {
					title: title,
					desc: desc,
					type: 'text',
					id: objectId
				};
				return Restangular.all('objects').patch(data, {
					access_token: token
				})
			};
			//add link object
		self.addLinkObject = function(title, desc, url, conceptId) {
				var data = {
					title: title,
					desc: desc,
					url: url,
					type: 'link',
					conceptId: conceptId
				};
				return Restangular.all('objects').customPOST(data, undefined, {
					access_token: token
				})
			};
			//Edit link object
		self.editLinkObject = function(title, desc, url, objectId) {
				var data = {
					title: title,
					desc: desc,
					url: url,
					type: 'link',
					id: objectId
				};
				return Restangular.all('objects').patch(data, {
					access_token: token
				})
			};
			//add doc object
		self.addDocObject = function(title, desc, file, conceptId) {
				var data = new FormData();
				data.append('title', title);
				data.append('desc', desc);
				data.append('file', file);
				data.append('conceptId', conceptId);
				data.append('type', 'file');
				return Restangular.all("objects").withHttpConfig({
					transformRequest: angular.identity
				}).customPOST(data, undefined, {
					access_token: token
				}, {
					'Content-Type': undefined
				});
			};
			//edit doc object
		self.editDocObject = function(title, desc, file, objectId) {
			var data = new FormData();
			data.append('title', title);
			data.append('desc', desc);
			data.append('file', file);
			data.append('id', objectId);
			data.append('type', 'file');
			return Restangular.all("objects").withHttpConfig({
				transformRequest: angular.identity
			}).patch(data, {
				access_token: token
			}, {
				'Content-Type': undefined
			});
		};
		self.editObject = function(object) {
				var data = {
					pos: object.pos,
					id: object.id
				};
				return Restangular.all('objects').patch(data, {
					access_token: token
				});
			};
			//get preview for link object
		self.getPreview = function(url) {
			return Restangular.all('').customGET('preview', {
				url: url
			});
		};
		return self;
	}
})();
