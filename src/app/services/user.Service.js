(function() {
    angular.module('pyoopilFrontend').service('userService', userService);

    function userService(localStorageService, $rootScope) {
        var self = this;
        self.token = '';
        self.name = '';
        self.avatarThumb = '';
        self.avatar = '';
        self.username = '';
        self.userId = '';
        self.saveLoggedInUser = function(response) {
            localStorageService.add('token', response.data.access_token);
            localStorageService.add('avatarThumb', response.data.avatarThumb);
            localStorageService.add('name', response.data.name);
            localStorageService.add('avatar', response.data.avatar);
            localStorageService.add('username', response.data.username);
            localStorageService.add('userId', response.data._id);
            self.setParameters();
        };
        self.setParameters = function() {
            if (localStorageService.get('token')) {
                self.token = localStorageService.get('token');
                self.name = localStorageService.get('name');
                self.avatarThumb = localStorageService.get('avatarThumb');
                self.avatar = localStorageService.get('avatar');
                self.username = localStorageService.get('username');
                self.userId = localStorageService.get('userId');
                $rootScope.$broadcast('userAuthenticated');
            } else {
                self.token = '';
                self.name = '';
                self.avatarThumb = '';
                self.avatar = '';
                self.username = '';
                self.userId = '';
                $rootScope.$broadcast('userUnauthenticated');
           }
        };
        // self.setParameters();
        self.removeParameters = function() {
            localStorageService.clearAll();
            self.setParameters();
        }
    }
})();
