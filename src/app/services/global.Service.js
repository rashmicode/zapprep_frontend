(function() {
    angular.module('pyoopilFrontend').service('globalService', globalService);

    function globalService($http, localStorageService,$location) {
        var self = this;
        self.isAuthorised = false;
        self.apiBaseUrl = '';
        var testUrl='http://localhost:3000';
        var prodUrl='http://api.zapprep.in:3000';
        self.getIsAuthorised = function() {
            self.validateSession();
            return self.isAuthorised;
        };
        self.validateSession = function() {
            var token = localStorageService.get('token');
            if (token !== null && angular.isDefined(token)) {
                // $http.defaults.headers.common = {
                //     'access_token': token
                // };
                self.isAuthorised = true;
            } else {
                self.isAuthorised = false;
            }
        }
        self.getApiBaseUrl = function() {
            switch ($location.host()) {
                case 'localhost':
                    self.apiBaseUrl = testUrl;
                    break;
                case '188.166.226.12':
                    self.apiBaseUrl = testUrl;
                    break;
                case 'pyoopil.com':
                    self.apiBaseUrl = prodUrl;
                    break;
                default:
                    self.apiBaseUrl = prodUrl;
            }
            return self.apiBaseUrl;
        }
    }
})();
