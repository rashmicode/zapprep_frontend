(function() {
	angular.module('pyoopilFrontend').config(socialConfig);

	function socialConfig($authProvider) {
		$authProvider.baseUrl = null;
		$authProvider.facebook({
			clientId: '1621300288195138',
			// responseType: 'token',
			url: 'http://api.pyoopil.com:3000/user/auth/facebook2',
			scope: ['email', 'public_profile', 'user_friends']
		});
		$authProvider.google({
			clientId: '737956463439-0m1295d2dd2nsrdg8535ine1p26ee5q4.apps.googleusercontent.com',
			url: 'http://api.pyoopil.com:3000/user/auth/google2'
				// scope:['email','profile']
		});
	}
})();
