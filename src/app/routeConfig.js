(function() {
  'use strict';
  angular.module('pyoopilFrontend').config(routerConfig);
  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.hashPrefix('!');
    $urlRouterProvider.otherwise('/login');
    $stateProvider.state('login', {
      url: '/login',
      templateUrl: 'app/views/loginOrSignup.html',
      onEnter: function(globalService, $rootScope) {
        if (globalService.getIsAuthorised()) {
          $rootScope.$broadcast('userAuthenticated');
        }
      }
    }).state('forgotPassword', {
      url: '/forgotPassword',
      templateUrl: 'app/views/forgotPassword.html',
      controller:'forgotPasswordCtrl',
      controllerAs:'forgotPassword'
    }).state('main', {
      abstract: true,
      url: '/',
      templateUrl: 'app/views/main.html',
      onEnter: function(globalService, $rootScope) {
        if (!globalService.getIsAuthorised()) {
          $rootScope.$broadcast('userUnauthenticated');
        }
      }
    }).state('main.dashboard', {
      url: 'dashboard',
      templateUrl: 'app/views/dashboard.html',
      controller: 'dashboardCtrl',
      controllerAs: 'dashboard'
    }).state('main.createCourse', {
      abstract: true,
      url: 'createCourse/',
      templateUrl: 'app/views/createCourse.html',
      controller: 'createCourseCtrl',
      controllerAs: 'createCourse'
    }).state('main.createCourse.step1', {
      url: 'step1',
      templateUrl: 'app/views/createCourseStep1.html',
      controller: function($scope) {
        if($scope.createCourse.step1FormCopy){
          delete $scope.createCourse.step1FormCopy;
        }
        $scope.createCourse.checkIsStep1();
      }
    }).state('main.createCourse.step2', {
      url: 'step2',
      templateUrl: 'app/views/createCourseStep2.html',
      controller: function($scope,$state) {
        if (!$scope.createCourse.step1FormCopy) {
          $state.go('main.createCourse.step1');
        } else {
          $scope.createCourse.checkIsStep1();
        }
      }
    }).state('main.addConcepts',{
      url:':pathId/addConcepts',
      templateUrl:'app/views/addConcepts.html',
      controller:'addConceptsCtrl',
      controllerAs:'addConcepts'
    });
  }
})();