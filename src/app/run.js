(function() {
	'use strict';
	angular.module('pyoopilFrontend').run(runBlock);
	/** @ngInject */
	function runBlock(Restangular, globalService, userService, $rootScope, $state,toastr) {
		Restangular.setBaseUrl(globalService.getApiBaseUrl());
		Restangular.setErrorInterceptor(function(response) {
		  if(response.status == 401) {userService.removeParameters();$state.transitionTo('login');}
		  if (response.data && response.data.errors) {
				toastr.error(response.data.errors[0]);
			}
      return false;
    });
		if (globalService.getIsAuthorised()) {
			userService.setParameters();
		}
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
if (!globalService.getIsAuthorised() && toState.name !== 'login') {
        $rootScope.$broadcast('userUnauthenticated');
      }
		})
	}
})();

