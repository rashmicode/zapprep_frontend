(function() {
	angular.module('pyoopilFrontend').config(themingConfig);
	//theme setting
	function themingConfig($mdThemingProvider) {
		$mdThemingProvider.theme('default').primaryPalette('orange', {
				'default': '500',
				'hue-1': '100',
				'hue-2': '700'
			})
			// If you specify less than all of the keys, it will inherit from the
			// default shades
			.accentPalette('deep-purple', {
				'default': 'A200' // use shade 200 for default, and keep all other shades the same
			});
 
	}
})();