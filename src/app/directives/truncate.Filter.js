(function() {
	angular.module('pyoopilFrontend').filter('truncate', function() {
		return function(text, length, end) {
			if (isNaN(length)) length = 10;
			if (angular.isUndefined(end)) end = "...";
			if (text.length <= length) {
				return text;
			} else {
				return String(text).substring(0, length - end.length) + end;
			}
		};
	});
})();