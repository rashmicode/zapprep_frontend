angular.module('pyoopilFrontend').directive('youtube', function($sce,youtubeRegex) {
  return {
    restrict: 'EA',
    scope: {
      code: '=',
      height:'='
    },
    replace: true,
    template: '<div ng-style="{height:height}"><iframe style="overflow:hidden;height:100%;width:100%" ng-src="{{url}}" frameborder="0" allowfullscreen></iframe></div>',
    link: function(scope) {
      scope.$watch('code', function(url) {
        if (url) {
          var id= (url.match(youtubeRegex)) ? RegExp.$1 : false;
          if(id)
          scope.url = $sce.trustAsResourceUrl("http://www.youtube.com/embed/" + id);
        }
      });
    }
  };
});