(function() {
  angular.module('pyoopilFrontend').directive('fileModel', fileModelDirective);

  function fileModelDirective($parse) {
    var helper = {
      isSizeLimit: function(file) {
        return file.size < 5242880;
      },
      isimage: function(file) {
        var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|'.indexOf(type) !== -1;
      },
      isdoc: function(file) {
        var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
        return true;
      }
    }
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        // var modelSetter = model.assign;
        var models = [];
        element.bind('change', function() {
          scope.$apply(function() {
            var errors = {
              filesize: [],
              incorrectFile: []
            };
            if (element[0].multiple == true) {
              for (var i = 0; i < element[0].files.length; i++) {
                if (helper["is" + attrs.fileType](element[0].files[i])) {
                  if (helper.isSizeLimit(element[0].files[i])) {
                    models.push(element[0].files[i]);
                  } else {
                    errors.filesize.push(element[0].files[i].name);
                  }
                } else {
                  errors.incorrectFile.push(element[0].files[i].name);
                }
              }
              element[0].value = "";
              model(scope, {
                $files: models,
                $errors: errors
              });
            } else {
              models = [];
              if (helper["is" + attrs.fileType](element[0].files[0])) {
                if (helper.isSizeLimit(element[0].files[0])) {
                  models.push(element[0].files[0]);
                } else {
                  errors.filesize.push(element[0].files[0].name);
                }
              } else {
                errors.incorrectFile.push(element[0].files[0].name);
              }
              element[0].value = "";
              model(scope, {
                $files: models,
                $errors: errors
              });
            }
          });
        });
      }
    };
  }
})();