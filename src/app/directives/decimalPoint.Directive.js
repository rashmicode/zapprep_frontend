angular.module('pyoopilFrontend').directive("decimals", function() {
	return {
		restrict: "A", // Only usable as an attribute of another HTML element
		require: "?ngModel",
		scope: {
			decimals: "@",
			decimalPoint: "@",
			minValue: "@"
		},
		link: function(scope, element, attr, ngModel) {
			var decimalCount = parseInt(scope.decimals) || 2;
			var decimalPoint = scope.decimalPoint || ".";
			var minValue = scope.minValue || 0;
			// Run when the model is first rendered and when the model is changed from code
			ngModel.$render = function() {
					// if (ngModel.$modelValue != null && ngModel.$modelValue < minValue) {
					//     ngModel = minValue;
					//     element.val(minValue);
					// }
					if (ngModel.$modelValue != null && ngModel.$modelValue >= minValue) {
						if (angular.isNumber(decimalCount)) {
							element.val(parseFloat(ngModel.$modelValue).toFixed(decimalCount).replace(".",decimalPoint));
						}
						// else {
						//     element.val(ngModel.$modelValue.toString().replace(".", ","));
						// }
					}
				}
				// Run when the view value changes - after each keypress
				// The returned value is then written to the model
			ngModel.$parsers.unshift(function(newValue) {
				if (angular.isNumber(decimalCount)) {
					var floatValue = parseFloat(newValue.replace(",", "."));
					if (decimalCount === 0) {
						return parseInt(floatValue);
					}
					return parseFloat(floatValue.toFixed(decimalCount));
				}
				return parseFloat(newValue.replace(",", "."));
			});
			// Formats the displayed value when the input field loses focus
			element.on("change", function() {
				var floatValue = parseFloat(element.val().replace(",", "."));
				if (!isNaN(floatValue) && angular.isNumber(decimalCount)) {
					if (floatValue < minValue) {
						ngModel.$setViewValue(minValue);
						element.val(minValue);
					} else {
						if (decimalCount === 0) {
							element.val(parseInt(floatValue));
						} else {
							var strValue = floatValue.toFixed(decimalCount);
							element.val(strValue.replace(".", decimalPoint));
						}
					}
				}
			});
		}
	}
});