(function() {
	'use strict';
	angular.module('pyoopilFrontend', [
		'angular-loading-bar',
		'ngAnimate',
		'ngCookies',
		'ngTouch',
		'ngSanitize',
		'ngMessages',
		'ngAria',
		'ui.router',
		'ngMaterial',
		'toastr',
		'restangular',
		'LocalStorageModule',
		'ui.validate',
		'satellizer',
		'dndLists'
		]);
})();