angular
.module('pyoopilFrontend')
.constant('youtubeRegex', /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/)
.constant('emailRegex',/\S+@\S+\.\S+/);